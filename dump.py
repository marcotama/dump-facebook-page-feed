import facepy
import json
import argparse
import pandas


def parse_entry(entry):
  if 'message' in entry:
      return {
          'post': entry['message'],
          'author': entry['from']['name'] if 'from' in entry and 'name' in entry['from'] else 'Unknown',
          'time': entry['created_time'],
          'likes': entry['likes']['summary']['total_count']
      }
  else:
      return None


def parse_entries(entries):
    posts = []
    for entry in entries:
        parsed_entry = parse_entry(entry)
        if parsed_entry is not None:
            posts.append(parsed_entry)
    return posts


def load_feed(group_id, graph):
    posts = []
    iterator = graph.get(
        group_id + "/feed",
        page=True,
        retry=3,
        limit=100,
        fields='message,from,created_time,likes.limit(1).summary(true)'
    )
    for i, page in enumerate(iterator):
        posts += parse_entries(page['data'])
        print("Pages loaded: %d        Total entries so far: %d" % (i, len(posts)), end="\r")
    print(' ' * 80, end='\r')
    print("Loaded %d entries" % len(posts))
    return posts


def agg_by_likes(df):
    import pandasql as pdsql
    pysql = lambda q: pdsql.sqldf(q, globals())
    pysql('SELECT author, SUM(likes) FROM df GROUP BY author ORDER BY SUM(likes) DESC').to_csv('top_posters.csv')


def dump(args):
    graph = facepy.GraphAPI(args.token)  # https://facepy.readthedocs.org/en/latest/usage/graph-api.html
    posts = load_feed(args.id, graph)
    with open(args.json, 'w') as outfile:
        json.dump(posts, outfile, indent = 4)
    df = pandas.DataFrame(posts)
    df.to_csv(args.csv)
    df.to_excel(args.xlsx)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('id', help='ID of Graph API resource')
    parser.add_argument('-j', '--json', default="content.json", help='Json output file')
    parser.add_argument('-c', '--csv', default="content.csv", help='Csv output file')
    parser.add_argument('-x', '--xlsx', default="content.xlsx", help='Xlsx output file')
    parser.add_argument('-t', '--token', help='Authentication token (get one here: https://developers.facebook.com/tools/explorer)')
    dump(parser.parse_args())