import pandas
df = pandas.read_json("content.json")
df.to_csv("content.csv")
df.to_excel("content.xlsx")

import pandasql as pdsql
pysql = lambda q: pdsql.sqldf(q, globals())
pysql('SELECT author, SUM(likes) FROM df GROUP BY author ORDER BY SUM(likes) DESC').to_csv('top_posters.csv')
pysql('SELECT post, author, likes FROM df ORDER BY likes DESC').to_csv('top_barze.csv')
