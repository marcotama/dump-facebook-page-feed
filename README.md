### What is this repository for? ###
Script to download all posts (along with author and likes count) of a Facebook page.

### How do I get set up? ###
First, you will need Python 3. On Debian systems you can easily to this with `sudo apt-get install python3 python3-pip`

You will also need to install Json support, Pandas, Argparse and FacePy: `sudo pip3 install json pandas argparse facepy`

To run the (at the moment) only script: `python3 dump.py <Facebook element graph ID> -t <authentication token>`

Notice that the element ID must be numeric (i.e., you cannot use the readable name).
To get an authorization token go here: https://developers.facebook.com/tools/explorer
In my limited experience, to download the posts of a page, you need to be admin of the page.

### Contribution guidelines ###
Feel free to expand on this.

### Who do I talk to? ###
Send me an email at tamassia.marco@gmail.com